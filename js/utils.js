function showList(){
    document.getElementById("commerceList").hidden = false;
}
function hideList(){
    document.getElementById("commerceList").hidden = true;
}
function HandleCounter(type,id){
    let quantity = Number(document.getElementById(id+"Quantity").children[0].children[1].innerHTML);

    //type: inc/dec
    switch(type){
        case 'inc':
            //Enable - sign
            document.getElementById(id+"Quantity").children[0].children[0].hidden = false;
            //Enable Delete
            document.getElementById(id).children[3].children[0].hidden = false;
            quantity++;
            document.getElementById(id+"Quantity").children[0].children[1].innerHTML = Number(quantity);
            break;
        case 'dec':
            if (quantity) {
                quantity--;
                document.getElementById(id+"Quantity").children[0].children[1].innerHTML = Number(quantity);
            }

            if (!quantity) {
                //Disable - sign
                document.getElementById(id+"Quantity").children[0].children[0].hidden = true;
                //Disable Delete
                document.getElementById(id).children[3].children[0].hidden = true;   
            }
        break;
    }
}

function ResetCounter(id){
    //Disable - sign
    document.getElementById(id+"Quantity").children[0].children[0].hidden = true;
    //Disable Delete
    document.getElementById(id).children[3].children[0].hidden = true;

    document.getElementById(id+"Quantity").children[0].children[1].innerHTML = Number(0);
}