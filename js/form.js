function hideElement(id){
    document.getElementById(id).hidden = true;
}
function showElement(id){
    document.getElementById(id).hidden = false;
}
function setElementText(id,text){
    document.getElementById(id).innerHTML = text;
}
function setClassToElement(id,className){
    let cl = document.getElementById(id).classList;
    cl.add(className);
    document.getElementById(id).classList = cl;
}
function removeClassFromElement(id,className){
    let cl = document.getElementById(id).classList;
    cl.remove(className);
    document.getElementById(id).classList = cl;
}

function removeAllClassesFromElement(id){
    document.getElementById(id).classList = [];
}
function validateName(){
    let returnValue = true;
    //Comprobarem que l'usuari hagi posat com a mínim un caràcter
    let str = document.getElementById('name').value;
    //clean error message
    hideElement('nameError');
    if(str.length <= 0){
        returnValue = false;
        //print error message
        showElement('nameError');
    }
    return returnValue;
}
function validateSurname(){
    let returnValue = true;
    let str = document.getElementById('surname').value;
    //clean error message
    hideElement('surnameError');

    if(str.length <= 0){
        returnValue = false;
        //print error message
        showElement('surnameError');

    }
    return returnValue;
}
function validateAddress(){
    let returnValue = true;
    let str = document.getElementById('address').value;
    //clean error message
    hideElement('addressError');

    if(str.length <= 0){
        returnValue = false;
        //print error message
        showElement('addressError');

    }
    return returnValue;
}
function validateBirthdate(){
    let returnValue = true;
    //Apliquem expresió regular per a les dates
    let pattern = new RegExp("^[0-9]{4}-[0-9]{2}-[0-9]{2}$");
    let str = document.getElementById('birthdate').value;
    //clean error message
    hideElement('birthdateError');

    if(!pattern.test(str)){
        returnValue = false;
        //print error message
        showElement('birthdateError');

    }
    return returnValue;
}
function validateSex(){
    let returnValue = true;
    // Guardem els tres checks en un array de bool.
    let checks = [
        document.getElementById('maleSex').checked,
        document.getElementById('femaleSex').checked,
        document.getElementById('nonBinarySex').checked
    ]
    //clean error message
    hideElement('sexError');

    let found = false;
    checks.forEach(itemValue => {
        if (found && itemValue){
            //print error message (2 trues?)
            showElement('sexError');
            returnValue = false;
        }
        if(itemValue) {
            found = true;
        }
    })
    if (!found){
        returnValue = false;
        showElement('sexError');

        //print error message
    }
    return returnValue;
}
function validateEmail(){
    let returnValue = true;
    //Apliquem expresió regular per a les dates
    let pattern = new RegExp("[^\s@]+@[^\s@]+\.[^\s@]+");
    let str = document.getElementById('email').value;
    //clean error message
    hideElement('emailError');

    if(!pattern.test(str)){
        returnValue = false;
        //print error message
        showElement('emailError');
    }
    return returnValue;
}



function validatePassword(){

    //clean error message
    hideElement('passwdError');
    removeAllClassesFromElement('passwdError');
    setClassToElement('passwdError','caption');

    var strongRegex = new RegExp("^(?=.{14,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{8,}).*", "g");
    var pwd = document.getElementById("passwd");
    if (pwd.value.length == 0) {
        setElementText('passwdError','Introdueix una contrasenya');
        setClassToElement('passwdError','error');
        showElement('passwdError');
        return false;

    } else if (false == enoughRegex.test(pwd.value)) {
        setElementText('passwdError','Necessites més caràcters');
        setClassToElement('passwdError','error');

        showElement('passwdError');
        return false;
    } else if (strongRegex.test(pwd.value)) {
        setElementText('passwdError','Contrasenya Forta!');
        setClassToElement('passwdError','success');

        showElement('passwdError');
        return true;
    } else if (mediumRegex.test(pwd.value)) {
        setClassToElement('passwdError','success');

        setElementText('passwdError','Contrasenya normal');
        showElement('passwdError');
        return true;
    } else {
        setClassToElement('passwdError','warning');

        setElementText('passwdError','Contrasenya poc fiable...');
        showElement('passwdError');
        return true;
    }


}
function validateAgreement(){
    //clean error message
    hideElement('agreementError');

    let agreement = document.getElementById('agreement').checked
    if(agreement) return agreement
    //print error message
    showElement('agreementError');


}
function validateForm(event){
    event.preventDefault();
    let validName = validateName();
    let validSurname = validateSurname();
    let validAddress = validateAddress();
    let validBirthdate = validateBirthdate();
    let validSex = validateSex();
    let validEmail = validateEmail();
    let validPassword = validatePassword();
    let validAgreement = validateAgreement();
    if(
        validName &&
        validSurname &&
        validAddress &&
        validBirthdate &&
        validSex &&
        validEmail &&
        validPassword &&
        validAgreement
    ){
        alert("Gràcies per posar-vos en contacte!");
        window.location = "index.html"
    } 
}